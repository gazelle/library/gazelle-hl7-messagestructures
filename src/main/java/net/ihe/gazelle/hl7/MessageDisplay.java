package net.ihe.gazelle.hl7;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.richfaces.model.TreeNode;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.DefaultXMLParser;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.parser.XMLParser;
import net.ihe.gazelle.common.tree.XmlTreeDataBuilder;

public class MessageDisplay {

	static {
		System.setProperty(DOMImplementationRegistry.PROPERTY,"org.apache.xerces.dom.DOMImplementationSourceImpl");
	}

	@SuppressWarnings("unchecked")
	public static TreeNode getMessageContentAsTree(Message hapiMessage, String encoding) throws HL7Exception, IOException, SAXException
	{
		if (hapiMessage == null) return null;
		if (encoding == null)
		{
			encoding = "UTF-8";
		}
		String xmlContent = getMessageContentAsXML(hapiMessage);
		TreeNode node = XmlTreeDataBuilder.build(new InputSource(new ByteArrayInputStream(xmlContent.getBytes(encoding))));
		return node;
	}

	public static String getMessageContentAsXML(Message hapiMessage) throws HL7Exception
	{
		if (hapiMessage == null) {
			return null;
		} else{
			XMLParser xmlParser = DefaultXMLParser.getInstanceWithNoValidation();
			return xmlParser.encode(hapiMessage, "XML");
		}
	}
	
	public static String getMessageContentAsER7(Message hapiMessage){
		if (hapiMessage == null){
			return null;
		} else{
			PipeParser pipeParser = PipeParser.getInstanceWithNoValidation();
			try{
				String message = pipeParser.encode(hapiMessage);
				return getMessageContentAsER7(message);
			} catch(HL7Exception e){
				return null;
			}
		}
	}

	public static String getMessageContentAsER7(String messageContent)
	{
		if (messageContent == null) return null;
		
		messageContent = messageContent.replace("<", "&lt;");  
		messageContent = messageContent.replace(">", "&gt;"); 
		messageContent = messageContent.replace("&", "&amp;");    
		messageContent = messageContent.replace("#", "&#35;");  
		messageContent = messageContent.replace("~", "&#126;");
		messageContent = messageContent.replace("|", "&#124;");
		messageContent = messageContent.replace("^", "&#94;");
		messageContent = messageContent.replace("\n", "<br/>");
		messageContent = messageContent.replace("\r", "<br/>");
		return messageContent;
	}

	public static String getMessageContentAsHighlightedER7(String messageContent)
	{			
		if (messageContent == null) return null;
		
		messageContent = messageContent.replace("\n", "\r");
		
		Integer b = 0;		
		List<String> listSegment = new ArrayList<String>();
		
		Integer a = messageContent.indexOf("|");
		messageContent = messageContent.replace("<", "&lt;");  
		messageContent = messageContent.replace(">", "&gt;");  

		if((a < messageContent.length()) && (a!= -1))
		{
			String subString = messageContent.substring(b, a);
			messageContent = messageContent.replaceFirst(subString, ("<span style=\"color: purple;\">" + subString + "</span>"));

			while(a < messageContent.length() && a > 0 && b >= 0)
			{				
				a = messageContent.indexOf("\r", a+1);		

				if ((a+1 < messageContent.length()) && (a != -1) )
				{
					b = messageContent.indexOf("|", a+1);

					if ((b-a) == 4)
					{						
						subString = messageContent.substring(a, b);	

						if (!listSegment.contains(subString))
						{
							listSegment.add(subString);
							messageContent = messageContent.replaceAll(subString, ("\r<span style=\"color: purple;\">" + subString.substring(1, 4) + "</span>"));					
						}
					}
				}
				else
					a = -1;
			}
		}

		messageContent = messageContent.replace("&", "<span style=\"color: blue;\">&amp;</span>");    
		messageContent = messageContent.replace("#", "<span style=\"color: blue;\">&#35;</span>");  
		messageContent = messageContent.replace("~", "<span style=\"color: blue;\">&#126;</span>");
		messageContent = messageContent.replace("|", "<span style=\"color: blue;\">&#124;</span>");
		messageContent = messageContent.replace("^", "<span style=\"color: #009966;\">&#94;</span>");
		messageContent = messageContent.replace("\r", "<span style=\"color: #FF0000;  font-size:6px;\">[CR]</span><br/>");

		return messageContent;

	}

}
