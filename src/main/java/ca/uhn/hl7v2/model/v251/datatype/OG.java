/**
 * This class has been created for the needs of the LAW integration profile which pre-adopts the
 * OG datatype from HL7v2.8.1
 */

package ca.uhn.hl7v2.model.v251.datatype;

import ca.uhn.hl7v2.model.DataTypeException;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.Type;
import ca.uhn.hl7v2.model.AbstractComposite;


/**
 * <p>Represents an HL7 OG (Observation Sub-ID) data type. 
 * This type consists of the following components:</p>
 * <ul>
 * <li>Original (ST)
 * <li>Group (NM)
 * <li>Sequence (NM)
 * <li>Identifier (NM)
 * </ul>
 */
public class OG extends AbstractComposite {

    private Type[] data;

    /** 
     * Creates a new CE type
     */
    public OG(Message message) {
        super(message);
        init();
    }

    private void init() {
        data = new Type[6];    
        data[0] = new ST(getMessage());
        data[1] = new NM(getMessage());
        data[2] = new NM(getMessage());
        data[3] = new NM(getMessage());
    }


    /**
     * Returns an array containing the data elements.
     */
    public Type[] getComponents() { 
        return this.data; 
    }

    /**
     * Returns an individual data component.
     *
     * @param number The component number (0-indexed)
     * @throws DataTypeException if the given element number is out of range.
     */
    public Type getComponent(int number) throws DataTypeException { 

        try { 
            return this.data[number]; 
        } catch (ArrayIndexOutOfBoundsException e) { 
            throw new DataTypeException("Element " + number + " doesn't exist (Type " + getClass().getName() + " has only " + this.data.length + " components)"); 
        } 
    } 


    /**
     * Returns Original (component 1).  This is a convenience method that saves you from 
     * casting and handling an exception.
     */
    public ST getOriginal() {
       return getTyped(0, ST.class);
    }

    
    /**
     * Returns Original (component 1).  This is a convenience method that saves you from 
     * casting and handling an exception.
     */
    public ST getOg1_Original() {
       return getTyped(0, ST.class);
    }


    /**
     * Returns Group (component 2).  This is a convenience method that saves you from 
     * casting and handling an exception.
     */
    public NM getGroup() {
       return getTyped(1, NM.class);
    }

    
    /**
     * Returns Group (component 2).  This is a convenience method that saves you from 
     * casting and handling an exception.
     */
    public NM getOg2_Group() {
       return getTyped(1, NM.class);
    }


    /**
     * Returns Sequence (component 3).  This is a convenience method that saves you from 
     * casting and handling an exception.
     */
    public NM getSequence() {
       return getTyped(2, NM.class);
    }

    
    /**
     * Returns Sequence (component 3).  This is a convenience method that saves you from 
     * casting and handling an exception.
     */
    public NM getOg3_Sequence() {
       return getTyped(2, NM.class);
    }


    /**
     * Returns Identifier (component 4).  This is a convenience method that saves you from 
     * casting and handling an exception.
     */
    public NM getIdentifier() {
       return getTyped(3, NM.class);
    }

    
    /**
     * Returns Identifier (component 4).  This is a convenience method that saves you from 
     * casting and handling an exception.
     */
    public NM getOg4_Identifier() {
       return getTyped(3, NM.class);
    }

}

